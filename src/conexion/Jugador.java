/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * 
 */
public class Jugador {

    private final String TABLA = "jugador";
    private final String SQL_INSERT = "INSERT INTO " + TABLA + " (nombre,apellidos,nick) values(?,?,?)";
    private final String SQL_DELETE = "DELETE FROM " + TABLA + " where id=?";
    private final String SQL_QUERY = " SELECT * FROM " + TABLA + " where id=?";
    private final String SQL_UPDATE = "UPDATE " + TABLA + " set nombre=?, apellidos=?, nick=? where id=?";

    public boolean insertarJugador(String nombre, String apellidos, String nick) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_INSERT);
            st.setString(1, nombre);
            st.setString(2, apellidos);
            st.setString(3, nick);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error el insertar usuario " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public boolean eliminarJugador(String id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_DELETE);
            st.setString(1, id);
            int num = st.executeUpdate();
            if (num == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al eliminar " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public JugadorPOJO consultaJugador(String id) {
        Connection con = null;
        PreparedStatement st = null;
        JugadorPOJO pojo = new JugadorPOJO();
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_QUERY);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pojo.setIdjugador(rs.getInt("idjugador")); //lo que esta entre comillas tiene que ser igual a la base de datos.
                pojo.setNombre(rs.getString("nombre"));
                pojo.setApellidos(rs.getString("apellidos"));
                pojo.setNick(rs.getString("nick"));
                pojo.setFecha(rs.getDate("fecha"));
            }
            Conexion.close(rs);
        } catch (Exception e) {
            System.out.println("Error al consultar " + e);
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return pojo;
    }

    public boolean actualizaContaacto(String id, String nombre, String apellidos, String nick) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_UPDATE);
            st.setString(1, nombre);
            st.setString(2, apellidos);
            st.setString(3, nick);
            st.setString(4, id);
            int i = st.executeUpdate();
            if (i == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al actualizar " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }
}
