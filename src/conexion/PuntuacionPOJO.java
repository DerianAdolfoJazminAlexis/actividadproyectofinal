/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.util.Date;

/**
 *
 *
 * 
 */
public class PuntuacionPOJO {

    private int idpuntac;
    private int perdidos;
    private int ganados;
    private int apostados;
    private int total;
    private Date fecha;
    private int idJugador;

    public int getIdpuntac() {
        return idpuntac;
    }

    public void setIdpuntac(int idpuntac) {
        this.idpuntac = idpuntac;
    }

    public int getPerdidos() {
        return perdidos;
    }

    public void setPerdidos(int perdidos) {
        this.perdidos = perdidos;
    }

    public int getGanados() {
        return ganados;
    }

    public void setGanados(int ganados) {
        this.ganados = ganados;
    }

    public int getApostados() {
        return apostados;
    }

    public void setApostados(int apostados) {
        this.apostados = apostados;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(int idJugador) {
        this.idJugador = idJugador;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
