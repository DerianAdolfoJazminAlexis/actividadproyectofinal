/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * 
 */
public class Puntuacion {

    private final String TABLA = "puntacion";
    private final String SQL_INSERT = "INSERT INTO " + TABLA + " (perdidos,ganados,apostados,total,jugador_idjugador) values(?,?,?,?,?)";
    private final String SQL_DELETE = "DELETE FROM " + TABLA + " where id=?";
    private final String SQL_QUERY = " SELECT * FROM " + TABLA + " where id=?";
    private final String SQL_UPDATE = "UPDATE " + TABLA + " set perdidos=?, ganados=?, apostados=?, total=?, jugador_idjugador=? where id=?";

    public boolean insertarPuntuacion(int perdidos, int ganados, int apostados,int total, int jugador_id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_INSERT);
            st.setInt(1, perdidos);
            st.setInt(2, ganados);
            st.setInt(3, apostados);
            st.setInt(4, total);
            st.setInt(5, jugador_id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error el insertar usuario " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public boolean eliminarPuntuacion(String id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_DELETE);
            st.setString(1, id);
            int num = st.executeUpdate();
            if (num == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al eliminar " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public PuntuacionPOJO consultaPuntuacion(String id) {
        Connection con = null;
        PreparedStatement st = null;
        PuntuacionPOJO pojo = new PuntuacionPOJO();
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_QUERY);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pojo.setIdpuntac(rs.getInt("idpuntac")); //lo que esta entre comillas tiene que ser igual a la base de datos.
                pojo.setPerdidos(rs.getInt("perdidos"));
                pojo.setGanados(rs.getInt("ganados"));
                pojo.setApostados(rs.getInt("apostado"));
                pojo.setTotal(rs.getInt("total"));
                pojo.setFecha(rs.getDate("fecha"));
                pojo.setIdJugador(rs.getInt("jugador_idjugador"));
            }
            Conexion.close(rs);
        } catch (Exception e) {
            System.out.println("Error al consultar " + e);
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return pojo;
    }

    public boolean actualizaPuntuacion(int id, int perdidos, int ganados, int apostados,int total, int jugador_id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_UPDATE);
            st.setInt(1, perdidos);
            st.setInt(1, ganados);
            st.setInt(1, apostados);
            st.setInt(1, total);
            st.setInt(1, jugador_id);
            st.setInt(4, id);
            int i = st.executeUpdate();
            if (i == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al actualizar " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

}
