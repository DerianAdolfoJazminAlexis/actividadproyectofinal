package game.client;

import java.awt.Color;
import java.io.PrintWriter;
import java.util.concurrent.ExecutionException;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class CheckWorker extends SwingWorker<Boolean, Void> {

    private JLabel label;
    private JTextField credito;
    private JTextField apuesta;
    private JLabel boton;
    SwingWorker swingWorkerA;
    SwingWorker swingWorkerB;
    SwingWorker swingWorkerC;
    private PrintWriter pw;
    private Variables var;

    /**
     * Constructor de clase
     */
    CheckWorker(SwingWorker a, SwingWorker b, SwingWorker c, JLabel lb, JTextField cr, JTextField ap, JLabel btn, Variables var, PrintWriter pw) {
        this.swingWorkerA = a;
        this.swingWorkerB = b;
        this.swingWorkerC = c;
        label = lb;
        credito = cr;
        apuesta = ap;
        boton = btn;
        this.var = var;
        this.pw = pw;
    }

    @Override
    protected Boolean doInBackground() throws Exception {

        int val1 = (Integer) swingWorkerA.get();
        int val2 = (Integer) swingWorkerB.get();
        int val3 = (Integer) swingWorkerC.get();
        if (val1 == val2 && val2 == val3) {
            return true;//gana
        } else {
            return false;//pierde
        }
    }

    @Override
    protected void done() {
        try {
            //segun resultado de juego actualiza interfaz
            if (get()) {
                var.setCredito(var.getCredito() + var.getApuesta());
                var.setGanado(var.getGanado() + var.getApuesta());
                credito.setText("" + var.getCredito());
                label.setForeground(Color.yellow);
                label.setText("GANASTE!");
            } else {
                var.setCredito(var.getCredito() - var.getApuesta());
                var.setPerdido(var.getPerdido() + var.getApuesta());
                credito.setText("" + var.getCredito());
                label.setForeground(Color.red);
                label.setText("PERDISTE!");
            }
            pw.println(var.getNombre() + " " + var.getCredito() + " " + var.getPerdido() + " " + var.getGanado());
            boton.setEnabled(true);
        } catch (InterruptedException ex) {
            System.err.println("InterruptedException: " + ex.getMessage());
        } catch (ExecutionException ex) {
            System.err.println("ExecutionException: " + ex.getMessage());
        }
    }

    public Variables getVariable() {
        return var;
    }
}
