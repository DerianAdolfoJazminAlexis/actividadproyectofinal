package control.server;

public class Main {

    public static void main(String args[]) {
        Administrador adm = new Administrador();
        ControladorJuego c = new ControladorJuego(adm);
        adm.addEventos(c);
        adm.setVisible(true);
    }
}
