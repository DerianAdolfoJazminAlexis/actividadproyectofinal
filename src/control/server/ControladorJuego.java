package control.server;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorJuego implements ActionListener {

    public static int PUERTO = 4321;
    private Administrador adm;
    private boolean jugando;
    private ServerSocket server;
    private ArrayList<Jugador> jugadores;

    public ControladorJuego(Administrador adm) {
        this.adm = adm;
        jugadores = new ArrayList<>();
    }

    private synchronized Jugador newClient(String nombre) {
        int id = jugadores.size();
        Jugador j = new Jugador(nombre);
        jugadores.add(j);
        return j;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Component c = (Component) e.getSource();
        switch (c.getName()) {
            case "puerto":
                System.out.println("holi");
                String resp = JOptionPane.showInputDialog(adm, "Introduzca el nuevo puerto:", "Nuevo puerto", JOptionPane.PLAIN_MESSAGE);
                resp = resp.trim();
                try {
                    int x = Integer.parseInt(resp);
                    if (x < 0) {
                        throw new NumberFormatException();
                    }
                    PUERTO = x;
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(adm, "Número de puerto inválido", "Puerto no válido", JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "iniciar":
                if (!jugando) {
                    iniciarJuego();
                }
                break;
            case "detener":
                if (jugando) {
                    detenerJuego();
                }
                break;
            case "actualizar":
                if (jugando) {
                    actualizarTabla();
                }
                break;
            case "salir":
                if (jugando) {
                    detenerJuego();
                }
                System.exit(0);
        }
    }

    private void iniciarJuego() {
        try {
            jugadores.clear();
            server = new ServerSocket(PUERTO);
            System.out.print("El puerto utilizado por el socket es: ");
            System.out.println(server.getLocalPort());
            System.out.println("Los clientes deberan conectarse a este servidor usando este puerto");
            System.out.print("La direccion IP del servidor es: ");
            System.out.println(InetAddress.getLocalHost());
            adm.getBotonIniciar().setEnabled(false);
            adm.getItemPuerto().setEnabled(false);
            adm.getBotonActualizar().setEnabled(true);
            adm.getBotonDetener().setEnabled(true);
            jugando = true;
            new HiloServidor().start();
        } catch (IOException ex) {
            System.out.println("Error a la hora de abrir el puerto");
        }
    }

    private synchronized void actualizarTabla() {
        ArrayList<Jugador> jugs = (ArrayList<Jugador>) jugadores.clone();
        DefaultTableModel tableModel = (DefaultTableModel) adm.getjTable1().getModel();
        tableModel.setRowCount(0);
        tableModel.setColumnIdentifiers(new String[]{"Nombre", "Crédito", "Ganado", "Perdido"});
        for (int i = 0; i < jugs.size(); i++) {
            Jugador x = jugs.get(i);
            if (!x.isJugando()) {
                continue;
            }
            tableModel.addRow(getDataJugador(x));
        }
        adm.getjTable1().setModel(tableModel);
        tableModel.fireTableDataChanged();
    }

    private String[] getDataJugador(Jugador j) {
        String data[] = new String[4];
        data[0] = j.getNombre();
        data[1] = String.valueOf(j.getCredito());
        data[2] = String.valueOf(j.getGanado());
        data[3] = String.valueOf(j.getPerdido());
        return data;
    }

    private void detenerJuego() {
        jugando = false;
        adm.getBotonActualizar().setEnabled(false);
        adm.getBotonDetener().setEnabled(false);
        adm.getBotonIniciar().setEnabled(true);
        adm.getItemPuerto().setEnabled(true);
        try {
            server.close();
        } catch (IOException ex) {
            Logger.getLogger(ControladorJuego.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void logString(String mensaje) {
        adm.getAreaLog().append(mensaje + "\n");
    }

    class HiloServidor extends Thread {

        @Override
        public void run() {
            while (jugando && !server.isClosed()) {
                try {
                    System.out.println("Esperando jugador");
                    Socket con = server.accept();
                    new HiloCliente(con).start();
                } catch (IOException ex) {
                    System.out.println("Error en la comunicacion");
                }
            }
        }
    }

    class HiloCliente extends Thread {

        private Socket socket;
        private Jugador jugador;

        public HiloCliente(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            BufferedReader br;
            PrintWriter pw;
            String line, nombre = "", mensaje;
            try {
                pw = new PrintWriter(socket.getOutputStream(), true);
                br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                line = br.readLine().trim();
                String buff[] = line.split("\\s+");
                nombre = buff[0];
                int credito = Integer.parseInt(buff[1]);
                int perdido = Integer.parseInt(buff[2]);
                int ganado = Integer.parseInt(buff[3]);
                System.out.println(nombre);
                mensaje = String.format("El jugador %s se ha unido con crédito %d (%s).", nombre, credito, socket.getInetAddress().toString());
                logString(mensaje);
                jugador = newClient(nombre);
                jugador.setCredito(credito);
                jugador.setGanado(ganado);
                jugador.setPerdido(perdido);
                jugador.setJugando(true);
                actualizarTabla();
                while (jugando && (line = br.readLine()) != null) {
                    buff = line.trim().split("\\s+");
                    credito = Integer.parseInt(buff[1]);
                    perdido = Integer.parseInt(buff[2]);
                    ganado = Integer.parseInt(buff[3]);
                    jugador.setCredito(credito);
                    jugador.setGanado(ganado);
                    jugador.setPerdido(perdido);
                    mensaje = String.format("Usuario %s ha jugado (Crédito %d, Ganado %d, Perdido %d)", jugador.getNombre(), jugador.getCredito(), jugador.getGanado(), jugador.getPerdido());
                    logString(mensaje);
                    actualizarTabla();
                }
            } catch (Exception e) {
                //e.printStackTrace();
            } finally {
                if (jugador != null) {
                    mensaje = String.format("El jugador %s ha dejado de jugar.", nombre);
                    logString(mensaje);
                    jugador.setJugando(false);
                    actualizarTabla();
                }
                if (socket != null && !socket.isClosed()) {
                    try {
                        socket.close();
                    } catch (IOException ex) {
                        Logger.getLogger(ControladorJuego.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
